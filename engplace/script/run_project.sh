#!/bin/sh

NAME=engplace
SRCDIR=engplace

GUNICORN=/usr/bin/gunicorn_django
HOMEDIR=/home/webproject/www/python

VIRTUALENV=/home/webproject/www/envs/engplace_pub
PROJECTDIR=${HOMEDIR}/projects/engplace_dj_cms/${NAME}
SOCKFILE=/tmp/${NAME}.sock
MANAGE=${PROJECTDIR}/manage.py

. ${VIRTUALENV}/bin/activate
export LD_LIBRARY_PATH=/usr/local/lib
cd ${PROJECTDIR}
gunicorn --bind unix:/tmp/engplace.sock engplace.wsgi:application
#python ${MANAGE} run_gunicorn --timeout=300 -w 4 --bind unix:${SOCKFILE}
#python ${MANAGE} run_gunicorn 88.80.190.229:8000
