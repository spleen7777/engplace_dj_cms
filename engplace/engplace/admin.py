from django.contrib import admin
from .models import ContactForm
# Register your models here.

class ContactFormAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    search_fields = ['id', 'name', 'subject', 'message', 'email',]
    list_display = ('date_send', 'name', 'subject', 'email',)
    list_filter = ('date_send',)
    fields = ('id', 'date_send', 'name', 'subject', 'message', 'email', 'date',)

    class Meta:
        model = ContactForm

admin.site.register(ContactForm, ContactFormAdmin)