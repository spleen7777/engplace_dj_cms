from django.db import models
import datetime
# Create your models here.


class ContactForm(models.Model):
    name = models.CharField(help_text='Имя отправителя', default='', max_length=50)
    email = models.CharField(help_text='Email отправителя', default='', max_length=50)
    subject = models.CharField(help_text='Тема обращения', default='', max_length=100)
    message = models.TextField(help_text='Текст сообщения', default='')
    date = models.CharField(help_text='Дата', default='', max_length=50)
    date_send = models.DateTimeField(help_text='Дата отправления запроса', default=datetime.datetime.today)

    class Meta:
        verbose_name = "Обращение с сайта"
        verbose_name_plural = "Обращения с сайта"

    def __str__(self):
        return self.name + ' - ' +self.subject
