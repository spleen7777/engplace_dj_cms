from django.template.loader import render_to_string
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.views.generic.base import View
import json
from .models import ContactForm


class WelcomePageView(View):
    def get(self, request, *args, **kwargs):
        tmpl = 'HomePage/home_page_base.html'
        return render(request, tmpl)


class ContactFormView(View):
    def post(self, request, *args, **kwargs):
        paramlist = ['name', 'email', 'message', 'date', 'subject']
        values = {}
        for param in paramlist:
            if param in request.POST:
                values.update({param: request.POST[param]})

        emessage = render_to_string('HomePage/contact.html', values)

        try:
            instForm = ContactForm(name=request.POST.get('name'),
                                   subject=request.POST.get('subject'),
                                   date=request.POST.get('date'),
                                   email=request.POST.get('email'),
                                   message=request.POST.get('message'),
                                   )
            instForm.save()
        except:
            pass

        text = 'Your form was sent! <p>Thanks!</p>'
        try:
            if request.LANGUAGE_CODE == 'ru': text = 'Данные были успешно отправленны. <p>Спасибо за проявленный интерес.</p>'
        except:
            pass

        return HttpResponse(json.dumps({'text': text}))


def robot_txt(request):
    return render_to_response('robots.txt')
