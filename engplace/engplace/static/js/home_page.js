$(document).ready(function () {

    var csrftoken = getCookie('csrftoken');

    // using jQuery
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    /***************** Initiate Flexslider ******************/
    $('.flexslider').flexslider({
        animation: "slide"
    });

    $('#sandbox-container').datepicker({});


//Contact Form

    $('#submit').click(function () {
            validateAndPOST()
        }
    );

    function cleanForm() {
        $('input[name=name]').val('');
        $('input[name=email]').val('');
        $('textarea[name=message]').val('');
        $('input[name=date]').val('');
        $('input[name=subject]').val('');

        $('#errormessage').removeAttr('style').html('');

        $('.text-red').removeClass('text-red');


    }

    function postForm(formData) {
        $.ajax({
            type: "POST",
            url: "/contact_form/",
            data: formData,
            success: function (data) {
                req = JSON.parse(data);
                cleanForm();
                $('#errormessage').html(req['text']).delay(5000).fadeOut('slow')
            },
            error: function () {
                $('#errormessage').html('Connection Error').removeAttr('style').attr('style', 'color:red;')
            }
        });
    }

    function validateAndPOST() {
        var formData = {
            'name': $('input[name=name]').val(),
            'email': $('input[name=email]').val(),
            'message': $('textarea[name=message]').val(),
            'date': $('input[name=date]').val(),
            'subject': $('input[name=subject]').val(),
            'csrfmiddlewaretoken': csrftoken,
            'valid': true
        };

        var errorfieds = [];

        if (formData.name == null || formData.name == '') {
            formData.valid = false
            errorfieds.push('namefield')
        }

        if (formData.email == null || formData.email == '') {
            formData.valid = false
            errorfieds.push('emailfield')
        }

        if (formData.message == null || formData.message == '') {
            formData.valid = false
            errorfieds.push('messagefield')
        }

        if (formData.subject == null || formData.subject == '') {
            formData.valid = false;
            errorfieds.push('subjectfield')
        }

        if (formData.valid) {
            postForm(formData)
        }
        else {
            viewUnValid(errorfieds)
        }

    }

    /// подсветка незаполненных и обязательных полей
    function viewUnValid(errorfieds) {
        for (i = 0; i < errorfieds.length; i++) {
            var fieldid = errorfieds[i];
            $('#' + fieldid).addClass('text-red')
        }
        $('#errormessage').html('Заполните все поля формы').attr('style', 'color:red;')
    }
});